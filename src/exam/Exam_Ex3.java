package exam;

public class Exam_Ex3 {
    //рулон билетов с номерами от 000001 до 999999
    public static void main(String[] args) {
        int x1, x2, x3, x4, x5, x6;// для хранения разрядов
        int count = 0;// для хранения количества счастливых билетов
        // первый счастливый билет начнется с 100 001
        for (int i = 100001; i <= 999999; i++) {
            x1 = i / 100000;
            x2 = i / 10000 % 10;
            x3 = i / 1000 % 10;
            x4 = i / 100 % 10;
            x5 = i / 10 % 10;
            x6 = i % 10;
            if ((x1 + x2 + x3) == (x4 + x5 + x6)) {
                count++;
            }
        }
        System.out.println("количество счастливых билетов:" + count);

    }
}
