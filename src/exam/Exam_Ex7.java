package exam;

public class Exam_Ex7 {
    public static void main(String[] args) {
        Pair<Integer, String> pair1 = new Pair<>(23, "hello");

        // возвращаем 1 и 2-й элементы
        System.out.println("first value:" + pair1.getFirstValue());
        System.out.println("second value:" + pair1.getSecondValue());

        //используем метод swap
        Pair<String, Integer> swappedPair = pair1.swap();
        System.out.println("swappedPair:" + swappedPair);

        // метод replaceFirst()
        Pair<Integer, String> pairAfterReplaceFirst = pair1.replaceFirst(78);
        System.out.println("pairAfterReplaceFirst: " + pairAfterReplaceFirst);

        // метод replaceLast()
        Pair<Integer, String> pairAfterReplaceLast = pair1.replaceLast("Bye");
        System.out.println("pairAfterReplaceLast: " + pairAfterReplaceLast);
    }

    public static class Pair<F, S> {
        private F firstValue;
        private S secondValue;

        public Pair(F firstValue, S secondValue) {
            this.firstValue = firstValue;
            this.secondValue = secondValue;
        }

        public void setFirstValue(F firstValue) {
            this.firstValue = firstValue;
        }

        public void setSecondValue(S secondValue) {
            this.secondValue = secondValue;
        }

        public F getFirstValue() {
            return firstValue;
        }

        public S getSecondValue() {
            return secondValue;
        }

        public Pair<S, F> swap() {
            return new Pair(secondValue, firstValue);
            /*System.out.println("new firstValue: " + firstValue);
            System.out.println("new secondValue:" + secondValue);*/
        }

        public Pair<F, S> replaceFirst(F someFvalue) {
            return new Pair(someFvalue, secondValue);

        }

        public Pair<F,S> replaceLast(S someSvalue) {
            return new Pair(firstValue, someSvalue);
        }

        public String toString() {
            return "(" + firstValue + ", " + secondValue + ")";
        }
    }
}
