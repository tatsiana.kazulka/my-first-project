package exam;


import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Exam_Ex5 {//Найти уникальное количество слов в тексте(решение №1)/количество уникальных слов(решение №2)

    public static void main(String[] args) {
        //решение задачи (вариант1): подсчет уникального количества слов(через Set и через Stream)
        String text = "Listen to the news from today <> and read the text at the same time . Listen to - the news from today without reading the text.";
        String[] wordArray = text.toLowerCase().split("[^a-zA-Z0-9]+");//или regex=^[\\s,.&!()<>&;:-]+
        //через Set ( в него можно добавить только уникальные значения, без повторений)
       Set<String> wordSet=new HashSet<>();
       wordSet.addAll(Arrays.asList(wordArray));
        System.out.println("количество элементов без дубликатов(используя Set) = "+wordSet.size());

       // через стрим
        int countUnique = (int) Arrays.stream(wordArray).distinct().count();
        System.out.println("количество элементов без дубликатов(испльзуя Stream) = "+ + countUnique);


        //решение задачи (вариант2): подсчет количества уникальных слов
        int count = 0;//переменная для подсчета уникальных значений
        // используем стрим для сортировки
        Stream<String> myStream = Arrays.stream(wordArray);
        final List<String> wordArray2 = myStream.sorted().collect(Collectors.toList());

        //проходимся по отсортированному списку , подсчитывая уникальные
        for (int i = 1; i < wordArray2.size() - 1; i++) {
            if (!wordArray2.get(i).equals(wordArray2.get(i - 1)) && (!wordArray2.get(i).equals(wordArray2.get(i + 1)))) {
                count++;
            }
        }
        System.out.println("количество слов, которые встречаются только ОДИН раз в исходной строке = " + count);
    }
}












