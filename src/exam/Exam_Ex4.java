package exam;

public class Exam_Ex4 {
    public static void main(String[] args) {
        // создаем двумерный массив размера n
        int N = 10;//пишем любое положительное число
        int[][] arr = new int[N][N];
        // заполняеем диагонали нулями , все остальное - отрицательными
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (i == j) {//условие для диагонали нулей слева-направо
                    arr[i][j] = 0;
                } else if (i + j == N - 1) {///условие для диагонали нулей справа-налево
                    arr[i][j] = 0;
                } else {
                    arr[i][j] = (int) (Math.random() * 8 - 9);
                }
            }
        }
// согласно условию, перезаписываем часть ячеек на положительные значения
        // рассматриваем два случай: размер массива - четное/нечетное
        if (N % 2 == 0) {
            for (int i = 0; i < (N / 2 - 1); i++) {
                for (int j = i + 1; j < (N - 1 - i); j++) {
                    arr[i][j] = (int) (Math.random() * 9 + 1);
                }
            }
            for (int i = N - 1; i > (N / 2); i--) {
                for (int j = N - i; j < i; j++) {
                    arr[i][j] = (int) (Math.random() * 9 + 1);
                }
            }
        } else {
            for (int i = 0; i < (N / 2); i++) {
                for (int j = i + 1; j < N - 1 - i; j++) {
                    arr[i][j] = (int) (Math.random() * 9 + 1);
                }
            }
            for (int i = N - 1; i > (N / 2); i--) {
                for (int j = N - i; j < i; j++) {
                    arr[i][j] = (int) (Math.random() * 9 + 1);
                }

            }
        }
        //находим сумму элементов
        int sum = 0;

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                sum += arr[i][j];
                System.out.print(arr[i][j] + " ");

            }
            System.out.print("\n");
        }
        System.out.println("сумм элементов массива= " + sum);
// находим среднее арифметическое элементов, которые больше чем сумма элементов
        double average = 0;
        int sumForAverage = 0;
        int countForAverage = 0;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (arr[i][j] > sum) {
                    countForAverage++;
                    sumForAverage += arr[i][j];
                }
            }
        }

        if (countForAverage != 0) {
            average =(double) sumForAverage / countForAverage;
            System.out.print("среднее значение элементов, которые больше чем " + sum + " = ");
            System.out.printf("%.3f",average);
            System.out.println(" [количество эл-тов: " + countForAverage + ", их сумма: " + sumForAverage+"]");
        } else {
            System.out.println("элементов, которые больше чем " + sum + " нет");
        }
    }

}



