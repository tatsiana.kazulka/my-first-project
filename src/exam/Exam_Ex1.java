package exam;

import java.util.Scanner;

public class Exam_Ex1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter time in seconds =");
        long sec = scan.nextLong();
        boolean isTrue = true;//вводим флажок, чтобы выйти из цикла при верном вводе числа

        while (isTrue) {
            if (sec <= 0) {
                System.out.println("Time should > 0. Enter again");
                sec = scan.nextLong();
            } else {
                isTrue = false;
            }
        }

        int secFinal = (int) sec % 60;//количество секунд в итоговом времени
        long min = (sec - secFinal) / 60;
        int minFinal = (int) min % 60;//количество минут в итоговом времени
        long hours = (min - minFinal) / 60;//количество часов в итоговом времени
        int hoursFinal = (int) hours % 24;
        long days = (hours - hoursFinal) / 24;
        int daysFinal = (int) (days % 7);
        int weeks = (int) ((days - daysFinal) / 7);

        //конструкции if..if else для проверки окончаний

        if (weeks > 0) {
            if (weeks % 10 == 1 && weeks % 100 != 11) {
                System.out.print(weeks + " неделя ");
            } else if ((weeks % 10 == 2 && weeks % 100 != 12) || (weeks % 10 == 3 && weeks % 100 != 13) || (weeks % 10 == 4 && weeks % 100 != 14)) {
                System.out.print(weeks + " недели ");
            } else {
                System.out.print(weeks + " недель ");
            }
        }

        if (daysFinal % 10 == 1) {
            System.out.print(daysFinal + " сутки ");
        } else {
            System.out.print(daysFinal + " суток ");
        }

        if (hoursFinal != 0) {
            if (hoursFinal % 10 == 1 && hoursFinal % 100 != 11) {
                System.out.print(hoursFinal + " час ");
            } else if ((hoursFinal % 10 == 2 && hoursFinal % 100 != 12) || (hoursFinal % 10 == 3 && hoursFinal % 100 != 13) || (hoursFinal % 10 == 4 && hoursFinal % 100 != 14)) {
                System.out.print(hoursFinal + " часа ");
            } else {
                System.out.print(hoursFinal + " часов ");
            }
        }

        if (minFinal > 0) {
            if (minFinal % 10 == 1 && minFinal % 100 != 11) {
                System.out.print(minFinal + " минута ");
            } else if ((minFinal % 10 == 2 && minFinal % 100 != 12) || (minFinal % 10 == 3 && minFinal % 100 != 13) || (minFinal % 10 == 4 && minFinal % 100 != 14)) {
                System.out.print(minFinal + " минуты ");
            } else {
                System.out.print(minFinal + " минут ");
            }
        }
        if (secFinal != 0) {
            if (secFinal % 10 == 1 && secFinal % 100 != 11) {
                System.out.print(secFinal + " секунда ");
            } else if ((secFinal % 10 == 2 && secFinal % 100 != 12) || (secFinal % 10 == 3 && secFinal % 100 != 13) || (secFinal % 10 == 4 && secFinal % 100 != 14)) {
                System.out.print(secFinal + " секунды ");
            } else {
                System.out.print(secFinal + " секунд ");
            }
        }
    }

}



