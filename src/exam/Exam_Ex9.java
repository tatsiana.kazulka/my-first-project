package exam;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Exam_Ex9 {
    public static void main(String[] args) {
        //создаем два файла in1.txt и in2.txt и рандомно их заполняем по 1000 чисел от 1 до 100000
        try (
                FileWriter fw1 = new FileWriter("int1.txt");
                FileWriter fw2 = new FileWriter("int2.txt")) {
            for (int i = 0; i < 1000; i++) {
                fw1.write((int) new Random().nextInt(100001) + "\n");
            }
            for (int i = 0; i < 1000; i++) {
                fw2.write((int) new Random().nextInt(100001) + "\n");
            }

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        //читаем по очереди данные из файлов, сохраняем их в List, сортируем c помощью Stream
        List<Integer> nums = new ArrayList<>();
        try (BufferedReader bfr1 = new BufferedReader(new FileReader("int1.txt"));
             BufferedReader bfr2 = new BufferedReader(new FileReader("int2.txt"))) {
            String res1 = bfr1.readLine();
            while (res1 != null) {
                nums.add(Integer.parseInt(res1));
                res1 = bfr1.readLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try (BufferedReader bfr2 = new BufferedReader(new FileReader("int2.txt"))) {
            String res2 = bfr2.readLine();
            while (res2 != null) {
                nums.add(Integer.parseInt(res2));
                res2 = bfr2.readLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Collections.sort(nums);//сортируем по возрастанию

        // записываем отсортированный список в файл out.txt
        try (FileWriter fr3 = new FileWriter("out.txt")) {
            for (Integer element : nums) {
                fr3.write(element + "\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

