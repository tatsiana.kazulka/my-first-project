package exam.Exam_Ex6;

public class Flower {
    private String name;
    private double price;
    private String color;
    private int lifeTerm;

    public Flower() {
    }

    public Flower(String name, double price, String color, int lifeTerm) {
        this.name = name;
        this.price = price;
        this.color = color;
        this.lifeTerm = lifeTerm;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public String getColor() {
        return color;
    }

    public int getLifeTerm() {
        return lifeTerm;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setTerm(int term) {
        this.lifeTerm = term;
    }

    public String toString() {
        return "[" + name + "price:" + price + "color: " + color + "lifeTerm: " + lifeTerm + "]";
    }
}
