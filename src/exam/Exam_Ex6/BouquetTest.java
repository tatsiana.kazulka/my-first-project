package exam.Exam_Ex6;

import java.util.*;

public class BouquetTest {
    public static void main(String[] args) {

        // интерактивно составим букет ( через консольный ввод) , выбранные варианты сохраним в List
        System.out.println("Для составления букета введите название цветов: rose, tulip, iris, lilac. если букет собран, введите -  isEnough");
        Scanner scanner = new Scanner(System.in);

        List<Flower> bouquet = new ArrayList<>();

        boolean moreFlowers = false;
        String nameChosen = scanner.nextLine();

        while (!moreFlowers) {
            nameChosen = scanner.nextLine();
            switch (nameChosen) {
                case "rose" -> bouquet.add(new Rose());
                case "tulip" -> bouquet.add(new Tulip());
                case "iris" -> bouquet.add(new Iris());
                case "lilac" -> bouquet.add(new Lilac());
                case "isEnough" -> moreFlowers = true;
            }
        }
// проходим по букету циклом,считаем стоимость букета, срок жизни(через Math.max()) и цвета (сохраняем в Set)
        double sum = 0;
        int maxTermLife = 0;
        Set<String> colorsInBouquet = new HashSet<>();

        for (Flower item : bouquet) {
            sum += item.getPrice();
            maxTermLife = Math.max(maxTermLife, item.getLifeTerm());
            colorsInBouquet.add(item.getColor());
        }

        System.out.println("стоимость букета:" + sum);
        System.out.println("букет завянет через(в днях):" + maxTermLife);
        for (String element : colorsInBouquet) {
            System.out.print(element + ", ");
        }

    }
}