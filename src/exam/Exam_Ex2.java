package exam;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class Exam_Ex2 {
    public static void main(String[] args) {
        // создаем и заполняем массив
        int[] array = new int[100000];
        for (int i = 0; i < array.length; i++) {
            array[i] = i + 1;
        }

        // циклом ищем необходимые совпадения и подсчитываем их
        int count = 0;
        for (int i = 3; i < array.length; i ++) {
            if (array[i]==4 || array[i] % 10 == 4 || array[i] % 100 / 10 == 4 || array[i] % 1000 / 100 == 4 || array[i] % 10000 / 1000 == 4 || array[i] % 100000 / 10000 == 4 ||
                    array[i] % 100 == 13 || array[i] % 1000 / 10 == 13 || array[i] % 10000 / 100 == 13 || array[i] % 100000 / 1000 == 13)
                count++;
        }
        System.out.println("Всего номеров, которые придется исключить "+count);




    }
}
