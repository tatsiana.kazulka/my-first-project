package credit;

public class Credit_Ex7 {
    public static void main(String[] args) {
        //создаем массив (напр, на 13 элементов) и заполняем его рандомно , напр , от -50 до 50 включительно
        int[] myArray = new int[13];
        for (int i = 0; i < myArray.length; i++) {
            myArray[i] = (int) (Math.random() * 101 - 50);
        }
        System.out.print("исходный массив: ");
        for (int element : myArray) {
            System.out.print(element + ", ");
        }
        System.out.println();
        System.out.print("перевернутый массив:");

        for (int i = 0; i < myArray.length / 2; i++) {
            int temp = myArray[i];
            myArray[i] = myArray[myArray.length - 1 - i];
            myArray[myArray.length - 1 - i] = temp;
        }
        for (int element : myArray) {
            System.out.print(element + ", ");
        }
    }
}
