package credit;

import javax.swing.*;
import java.util.Scanner;

public class Credit_Ex4 {
    public static void main(String[] args) {
//простое число Х- это натуральное число, которое больше 1 и имеет ТОЛЬКО 2 делителя: 1 и Х
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите число (целое)=");
        double numEntered = scan.nextDouble();

// надо проверить на целое
        while ((numEntered % 1) > 0) {
            System.out.println("Ошибка! Введите целое число=");
            numEntered = scan.nextDouble();
        }

// находим количество делителей
        int delitel = 0;//количество делителей
        for (int i = 1; i <= (numEntered); i++) {
            if (numEntered % i == 0)
                delitel = delitel + 1;
        }
// надо проверить на >1 и на 2 делителя
        if (numEntered <= 1)
            System.out.println("Введено не простое число ( меньше либо равно 1)");
        else if (delitel > 2)
            System.out.println("Введено не простое (делителей больше 2)");
        else
            System.out.println("Введено простое число");


    }
}

