package credit;
import java.io.*;
import java.util.Arrays;
import java.util.Random;
public class Credit_Ex13 {
    public static void main(String[] args) {
        //запишем 30 случ числе в файл в диапазоне от 1 до 100 с помощью Random() и цикла for
        // для записи в двоичном стиле используем DataOutputStream
        try (FileOutputStream fos = new FileOutputStream("File_Ex13.dat");
             DataOutputStream dos = new DataOutputStream(fos)) {
            for (int i = 0; i < 30; i++) {
                dos.writeInt(new Random().nextInt(101) +1);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        try(FileInputStream fis=new FileInputStream("File_Ex13.dat");
        DataInputStream dis=new DataInputStream(fis)){
            int[] nums=new int[30];
            int sum=0;
            for (int i=0;i<30;i++){
                nums[i]=dis.readInt();
                sum+=nums[i];
            }
            System.out.println("читаем из файла:"+Arrays.toString(nums));
            System.out.println("average= "+sum/30);
        } catch(IOException e) {
                System.out.println(e.getMessage());
        }
        }
    }


