package credit;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Credit_Ex12 {
    // файл можно создать в корне проекта( тогда можно не прописывать путь)или вне проекта (тогда нужно прописать путь из командной строки+ учесть File.separator)
    //используем FileReader, т.к. он работает корректно и с русским текстом ( а FileInputStream - только с латиницей)
    public static void main(String[] args) {
        try (FileReader fr = new FileReader("Test_Ex12");
             BufferedReader br = new BufferedReader(fr)) {
            String line;
            String total = "";
            int count = 0;
            while ((line = br.readLine()) != null) {
                count++;
                total += line;
            }
            System.out.println("читаем из файла:" + total);

            // ищем количество слов
            String[] words = total.split("[^а-яА-Я_0-9]+");
            int countWords = 0;
            for (String word : words) {
                countWords++;
            }
            System.out.println("количество слов:" + countWords);

            // ищем знаки препинания
            String regex = "[.,;:?!-]";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(total);
            int countZnakov = 0;
            while (matcher.find()) {
                countZnakov++;
            }
            System.out.println("кол-во знаков препинания:" + countZnakov);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}

