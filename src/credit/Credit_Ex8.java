package credit;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Credit_Ex8 {
    public static void main(String[] args) {
        //справочно- всего в русском языке 10 знаков препинания(Точка, запятая, точка с запятой, двоеточие, многоточие,
        // восклицательный знак, вопросительный знак, тире, кавычки, скобки.)
        String myString = "Тестовая строка!!?() \" и \" найти все знаки препинания: точку. точку с запятой; двоеточние: троеточие...  тире - скобки(и тд)";
// ищем все знаки, кроме парных (""()), точки и многототочия
        String regex = "[,;:?!-]";
        Pattern pattern1 = Pattern.compile(regex);
        Matcher matcher1 = pattern1.matcher(myString);
        int count1 = 0;
        while (matcher1.find()) {
            count1++;
        }
        System.out.println("кол-во знаков без точек и парных знаков:" + count1);

        //ищем отдельно точку
        String regex2 = "[^.]\\.[^.]";
        Pattern pattern2 = Pattern.compile(regex2);
        Matcher matcher2 = pattern2.matcher(myString);
        int count2 = 0;
        while (matcher2.find()) {
            count2++;
        }
        System.out.println("кол-во точек:" + count2);

        // ищем троеточние
        String regex3 = "\\.";
        Pattern pattern3 = Pattern.compile(regex3);
        Matcher matcher3 = pattern3.matcher(myString);
        int count3 = 0;
        while (matcher3.find()) {
            count3++;
        }
        int totalMnogot=(count3 - count2) / 3;
        System.out.println("кол-во многоточий:" +totalMnogot);

        // ищем скобки ( как один знак препинания)
        String regex4 = "[()]";
        Pattern pattern4 = Pattern.compile(regex4);
        Matcher matcher4 = pattern4.matcher(myString);
        int count4 = 0;
        while (matcher4.find()) {
            count4++;
        }
        int totalSkobok=count4 / 2;
        System.out.println("кол-во скобок как один знак:" +totalSkobok );

        // ищем кавычки ( как один знак препинания)
        String regex5 = "\\\"";
        Pattern pattern5 = Pattern.compile(regex5);
        Matcher matcher5 = pattern5.matcher(myString);
        int count5 = 0;
        while (matcher5.find()) {
            count5++;
        }
        int totalKavychek=count5 / 2;
        System.out.println("кол-во дв.кавычек как один знак:" +totalKavychek);

        int total = count1 + count2 + totalMnogot+totalSkobok+ totalKavychek;
        System.out.println("всего знаков препинания: " + total);
    }
}


