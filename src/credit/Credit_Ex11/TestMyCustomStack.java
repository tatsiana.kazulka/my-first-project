package credit.Credit_Ex11;

public class TestMyCustomStack {
    public static void main(String[] args) {
        MyCustomStack myStack = new MyCustomStack();
        myStack.push(16);
        myStack.push(5);
        myStack.push(31);
        System.out.println("current max " + myStack.max());
        myStack.pop();
        System.out.println("current max " + myStack.max());
        myStack.push(28);
        System.out.println("current max " + myStack.max());
        myStack.pop();
        myStack.pop();
        myStack.push(56);
        myStack.push(138);
        System.out.println("current max " + myStack.max());
        myStack.pop();
        myStack.pop();
        System.out.println("current max " + myStack.max());
    }
}
