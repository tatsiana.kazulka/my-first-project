package credit.Credit_Ex11;

import java.util.ArrayList;
import java.util.Collections;
import java.util.ArrayDeque;

public class MyCustomStack extends ArrayDeque<Integer> {
    private ArrayList<Integer> myList = new ArrayList<>();//будем хранить элементы стека и сразу сортировать

    public void push(Integer a) {
        myList.add(a);
        Collections.sort(myList);
        super.push(a);
    }

    public Integer pop() {
        myList.remove(super.getFirst());//удаляем из myList элемент с заданным индексом(элемент из головы очереди)
        return super.pop();//удаляем из очереди
    }

    public boolean isEmpty() {
        return super.isEmpty();
    }

    public Integer max() {
        if (myList.isEmpty()) {
            return null;
        } else {
            return myList.get(myList.size() - 1);
        }
    }
}




