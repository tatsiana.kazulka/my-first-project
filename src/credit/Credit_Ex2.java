package credit;

import java.util.Scanner;

public class Credit_Ex2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

//вводим Год и проверям на верность формата ( должо быть не отрицательное и целое число)
        System.out.println("Введите год");
        double year = scan.nextDouble();

        boolean isTrue = true;//логическая переменная для выхода из цикла

        while (isTrue) {
            if (year <= 0 || (year % 1) > 0) {// проверка на отриц.число и на целое число
                System.out.println("Ошибка! Введите год еще раз:целое положительное число=");
                year = scan.nextDouble();
            } else {
                isTrue = false;
            }

        }

        // Проверка на високосность года (три ветвления):
        // 1) если год не делится на 4, то обычный,
        // 2) если делится на 4, то проверяем, делится ли на 100. если не делится на 100, то високосный
        // 3) если делится на 100, то проверяем , делится ли на 400: если делится, то високосный,
        // если не делится на 400, то обычный

        boolean visokos = true;// переменная для года ( високосный или обычный).

        if ((year % 4) > 0) {
            visokos = false;
        } else if (year % 100 > 0) {
            visokos = true;
        } else if (year % 400 == 0) {
            visokos = true;
        } else {
            visokos = false;
        }

//Вводим месяц и проверям на верность формата ( должо быть не отрицательное и целое число и диапазон [1:12])
        System.out.println("Введите месяц");
        double month = scan.nextDouble();
        isTrue = true;//логическая переменная для выхода из цикла

        while (isTrue) {
            if (month < 1 || month > 12 || (month % 1) > 0) {
                System.out.println("Ошибка! Введите месяц еще раз:целое число от 1 до 12 включительно =");
                month = scan.nextDouble();
            } else {
                isTrue = false;
            }

        }

        int max_kolvo_dnei = 0;// переменная для макс кол-ва дней
        //если месяц 1,3,5,7,8,10,12, то 31 день
        //если месяц=2, то 28 или 29 дней
        // иначе 30 дней

        if (month == 4 || month == 6 || month == 9 || month == 11) {
            max_kolvo_dnei = 30;
        } else if (month == 2) {
            if (visokos = true)
                max_kolvo_dnei = 29;
            else max_kolvo_dnei = 28;
        } else
            max_kolvo_dnei = 31;

//Вводим День и проверям на верность формата ( должо быть не отрицательное и целое число и диапазон [1:31])
        System.out.println("Введите день");
        double day = scan.nextDouble();

        isTrue = true;
        while (isTrue) {
            if (day <= 1 || day > max_kolvo_dnei || (day % 1) > 0) {
                System.out.println("Ошибка! Введите день еще раз: целое число от 1 до 28-31 включительно");
                day = scan.nextDouble();
            } else
                isTrue = false;

//переменные для даты, увеличенной на один день
            double dayNew = day;
            double monthNew = month;
            double yearNew = year;

            if (day == max_kolvo_dnei)// условие для дней при переходе через месяц
                dayNew = 1;
            else dayNew = day + 1;


            if (day == max_kolvo_dnei && month == 12)  // условие  для года и месяца при переходе между годами
            {
                yearNew = year + 1;
                monthNew = 1;
            } else if (day == max_kolvo_dnei && month != 12)
                monthNew = month + 1;
            else
                monthNew = month;

            System.out.println("год= " + (int) year);
            System.out.println("месяц= " + (int) month);
            System.out.println("день= " + (int) day);
            System.out.println((int) dayNew + " " + (int) monthNew + " " + (int) yearNew);

        }

    }
}




