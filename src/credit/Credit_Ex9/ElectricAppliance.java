package credit.Credit_Ex9;

public abstract class ElectricAppliance {
    protected String name;
    protected String type;// fir kitchen, living, clothes
    protected int watt;//мощность
    protected boolean state = false;

    public ElectricAppliance(String name, String type) {
        this.name = name;
        this.type = type;
        this.watt = 220;
    }

    //абстрактные методы
    abstract public void turnOn();// дополнительно проверить на включенность

    abstract public void turnOff();


    // геттеры и сеттеры


    public void setType(String type) {
        this.type = type;
    }


    public void setName(String name) {
        this.name = name;
    }

    public int getWatt() {
        return watt;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public String toSring() {
        return "[name: " + name + ", "
                + "type: " + type + ", "
                + "watt: " + watt + "] ";
    }
}


