package credit.Credit_Ex9;


public class Iron extends ElectricAppliance {
    private int numberOfPrograms;
    private boolean withSteamer;

    public Iron(String name, String type, int numberOfPrograms, boolean withSteamer) {
        super(name, type);
        this.numberOfPrograms = numberOfPrograms;
        this.withSteamer = withSteamer;
    }

    public int getNumberOfPrograms() {

        return numberOfPrograms;
    }

    public void setNumberOfPrograms(int numberOfPrograms) {

        this.numberOfPrograms = numberOfPrograms;
    }

    public void setWithSteamer(boolean withSteamer) {

        this.withSteamer = withSteamer;
    }

    public boolean isWithSteamer() {

        return withSteamer;
    }

    @Override
    public void turnOn() {
        this.state=true;
        System.out.println("Утюг включен");
    }



    @Override
    public void turnOff() {
        this.state=false;
        System.out.println("Утюг выключен");
    }


// переопределим метод String
@Override
public String toSring(){
        return"[name: "+name+", "
        +"type: "+type+", "
        +"watt: "+watt+", "
        +"numberOfPrograms: "+numberOfPrograms+", "
        +"withSteamer "+withSteamer+"]";
        }

        }

