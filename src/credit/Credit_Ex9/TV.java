package credit.Credit_Ex9;

public class TV extends ElectricAppliance {
    private String colorScreen;
    private double screenSize;

    public TV(String name, String type, String colorScreen, double screenSize) {
        super(name, type);
        this.colorScreen = colorScreen;
        this.screenSize = screenSize;
    }

    @Override
    public void turnOn() {
        this.state=true;
        System.out.println("Телевизор включен");
    }


    @Override
    public void turnOff() {
        this.state=false;
        System.out.println("Телевизор выключен");
    }

    // переопределим метод String
    @Override
    public String toSring() {
        return "[name: " + name + ", "
                + "type: " + type + ", "
                + "watt: " + watt + ", "
                + "colorScreen: " + colorScreen + ", "
                + "screenSize: " + screenSize + "]";
    }
}

