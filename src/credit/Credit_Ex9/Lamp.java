package credit.Credit_Ex9;

public class Lamp extends ElectricAppliance {

    private String material;
    private String color;
    private double height;


    public Lamp(String name, String type, String material, String color, double height) {
        super(name, type);
        this.material = material;
        this.color = color;
        this.height = height;
    }



    // геттеры и сеттеры
    public void setMaterial(String material) {
        this.material = material;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public String getMaterial() {
        return material;
    }

    public String getColor() {
        return color;
    }

    public double getHeight() {
        return height;
    }

    @Override
    public void turnOn() {
     this.state=true;
        System.out.println("Лампа включена");

    }


    @Override
    public void turnOff() {
        this.state=false;
        System.out.println("Лампа выключена");

    }

    // переопределим метод String
    @Override
    public String toSring() {
       return "[name: " + name + ", "
                + "type: " + type + ", "
                + "watt: " + watt + ", "
                + "material: " + material + ", "
                + "color " + color + ", "
                + "height " + height + "]";
    }
}


