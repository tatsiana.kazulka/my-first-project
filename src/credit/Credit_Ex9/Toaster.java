package credit.Credit_Ex9;

public class Toaster extends ElectricAppliance {
    private int numberOfToasts;
    private boolean canDefrost;

    public Toaster(String name, String type, int numberOfToasts, boolean canDefrost) {
        super(name, type);
        this.numberOfToasts = numberOfToasts;
        this.canDefrost = canDefrost;
    }


    public void setNumberOfToasts(int numberOfToasts) {
        this.numberOfToasts = numberOfToasts;
    }

    public void setCanDefrost(boolean canDefrost) {
        this.canDefrost = canDefrost;
    }

    public int getNumberOfToasts() {
        return numberOfToasts;
    }

    public boolean isCanDefrost() {
        return canDefrost;
    }

    @Override
    public void turnOn() {
        this.state=true;
        System.out.println("Тостер включен");
    }

    @Override
    public void turnOff() {
        this.state=false;
        System.out.println("Тостер выключен");
    }

    // переопределим метод String
    @Override
    public String toSring() {
        return "[name: " + name + ", "
                + "type: " + type + ", "
                + "watt: " + watt + ", "
                + "numberOfToasts: " + numberOfToasts + ", "
                + "canDefrost: " + canDefrost + "]";
    }
}

