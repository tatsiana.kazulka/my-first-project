package credit.Credit_Ex9;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class MainTestElectricAppliance {
    public static void main(String[] args) {

        ElectricAppliance toaster = new Toaster("toaster", "kitchen", 2, true);
        ElectricAppliance tv1 = new TV("TV", "living", "colored", 27.0);
        ElectricAppliance tv2 = new TV("TV", "living", "black-white", 15.0);
        ElectricAppliance tv3 = new TV("TV", "living", "colored", 16.0);
        ElectricAppliance lamp1 = new Lamp("lamp", "living", "plastic", "black", 16.0);
        ElectricAppliance lamp2 = new Lamp("lamp", "kitchen", "wooden", "white", 20.0);
        ElectricAppliance iron = new Iron("iron", "clothes", 4, true);

        List<ElectricAppliance> homeTechnic = new ArrayList<>();
        homeTechnic.add(toaster);
        homeTechnic.add(tv1);
        homeTechnic.add(tv2);
        homeTechnic.add(tv3);
        homeTechnic.add(lamp1);
        homeTechnic.add(lamp2);
        homeTechnic.add(iron);

// включаем несколько приборов

        toaster.turnOn();
        lamp1.turnOn();
        tv3.turnOn();



// считаем потребляемую мощность
        int totalWatt = 0;
        for (ElectricAppliance item : homeTechnic) {
            if (item.state) {
                totalWatt += item.getWatt();
            }
        }
        System.out.println("total Watt " + totalWatt);

        //сортировка с по параметру "назначение"
        System.out.println("сортировка по параметру \"назначение\" c помощью Comparator:");
        homeTechnic.sort(Comparator.comparing(ElectricAppliance::getType));
        for (ElectricAppliance item : homeTechnic)
        {
            System.out.print(item.getName()+" ");
        }

        System.out.println();
        System.out.println("ищем кухонный прибор, который включен:");
        // ищем прибор (кухонный, включен)
        homeTechnic.stream()
                .filter((i -> i.state == true))
                .filter((i -> i.getType() == "kitchen"))
                .forEach((i -> System.out.println("результат сортировки \"Включенный кухонный прибор\"    name: " + i.getName() + ", type: " + i.getType() + ", watt: " + i.getWatt())));
    }
}
