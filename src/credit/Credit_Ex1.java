package credit;

import java.util.Scanner;
import java.lang.Math;

public class Credit_Ex1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Введите 1-ую сторону 'a' прямоугольника =");
        double a = scan.nextDouble();
        boolean isTrue = true;// вводим доп. логич. переменную, чтобы могли выйти из цикла при верном вводе

        while (isTrue) {// проверяем, чтобы сторона была больше нуля
            if (a <= 0) {
                System.out.println("еще раз");
                a = scan.nextDouble();
            } else isTrue = false;
        }

        System.out.println("Введите 2-ую сторону 'b' прямоугольника =");
        double b = scan.nextDouble();
        isTrue = true;
        while (isTrue) {// проверяем, чтобы сторона была больше нуля
            if (b <= 0) {
                System.out.println("еще раз");
                b = scan.nextDouble();
            } else isTrue = false;
        }

        System.out.println("Введите 'r'  радиус окружности =");
        double r = scan.nextDouble();
        isTrue = true;
        while (isTrue) {// проверяем, чтобы радиус был больше нуля
            if (r <= 0) {
                System.out.println("еще раз");
                r = scan.nextDouble();
            } else isTrue = false;

        }

        if (r >= (Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2))) / 2) {
            System.out.println(a);
            System.out.println(b);
            System.out.println(r);
            System.out.println("Картонка с радиусом " + r + " закрывает полностью отверстие размера " + a + "*" + b);

        }else
        System.out.println("Картонка с радиусом " + r + " не закрывает полностью отверстие размера " + a + "*" + b);


    }
}



