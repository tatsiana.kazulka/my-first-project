package credit;

import java.util.Arrays;

public class Credit_Ex6 {
    public static void main(String[] args) {
        //создаем массив (напр, на 15 элементов) и заполняем его рандомно , напр , от -100 до 100
        int[] myArray = new int[15];
        for (int i = 0; i < myArray.length; i++) {
            myArray[i] = (int) (Math.random() * 201 - 100);
        }
        // для наглядности выведем массив
        for (int element : myArray) {
            System.out.print(element + ", ");
        }

        System.out.println();

        // найдем макс число, попарно сравнивая элементы
        int max = myArray[0];// переменная для хранения максимального значения
        for (int i = 1; i < myArray.length; i++) {
            if (max < myArray[i]) {
                max = myArray[i];
            }
        }
        for (int i = 0; i < myArray.length; i++) {// чтобы найти индекс максимального значения, сравним все элементы с полученным значением max
            if (myArray[i] == max) {
                System.out.println("макс число= " + max + ", индекс= " + i);
            }
        }
        // найдем мин число, попарно сравнивая элементы
        int min = myArray[0];
        for (int i = 1; i < myArray.length; i++) {
            if (min > myArray[i]) {
                min = myArray[i];
            }
        }
        for (int i = 0; i < myArray.length; i++) {// чтобы найти индекс минимального числ, сравним все элементы с полученным значением min
            if (myArray[i] == min) {
                System.out.println("мин число= " + min + ", индекс= " + i);
            }
        }
    }
}